#include "util/util.h"
#include <stdlib.h>

void free_image(struct image* image){
    free(image->data);
}
