#include "file_processing/open/bmp_file_open.h"
#include "file_processing/open/open_file_optional.h"

struct open_file_optional bmp_file_open(const char *filename)
{

    struct open_file_optional bmp_file_optional;

    FILE *file = fopen(filename, "rb");
    bmp_file_optional.file = file;

    if (file == NULL)
    {
        bmp_file_optional.status = OPEN_ERROR;
        return bmp_file_optional;
    }

    bmp_file_optional.status = OPEN_OK;
    return bmp_file_optional;
}
