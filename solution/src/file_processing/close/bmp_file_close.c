#include "file_processing/close/bmp_file_close.h"
#include <stdlib.h>


void bmp_file_close_print_error(char *message)
{
    fprintf(stderr, "ERROR: %s", message);
}

uint8_t bmp_file_close(FILE *file)
{   
    if (file == NULL){
        bmp_file_close_print_error("UNKNOWN FILE");
        return -1;
    }
    uint8_t code = fclose(file);
    switch (code)
    {
    case SEGMENTETION_FAULT:
        bmp_file_close_print_error("SEGMENTETION_FAULT");
    case IO_ERROR:
        bmp_file_close_print_error("IO_ERROR");
    default:
        if (code != CLOSE_OK)
        {
            bmp_file_close_print_error("UNEXPECTED ERROR");
        }
    return -1;
    }

    return code;
}
