#include "transformation/rotate_image.h"
#include <stdlib.h>
#include <string.h>

void copy_image_data(struct image *copy_image, struct image *original_image)
{
    for (uint64_t i = 0; i < original_image->width * original_image->height; i++)
    {
        copy_image->data[i] = original_image->data[i];
    }
}

void rotate_image_270(struct image *image, struct image source)
{
    for (uint64_t y = 0; y < source.height; y++)
    {
        for (uint64_t x = 0; x < source.width; x++)
        {
            uint64_t src_index = (y * source.width) + x;
            uint64_t dst_index = (x * source.height) + (source.height - y - 1);
            image->data[dst_index] = source.data[src_index];
        }
    }
    image->width = source.height;
    image->height = source.width;
}

void rotate_image_180(struct image *image, struct image source)
{
    for (uint64_t y = 0; y < source.height; y++)
    {
        for (uint64_t x = 0; x < source.width; x++)
        {
            uint64_t src_index = (y * source.width) + x;
            uint64_t dst_index = (source.width * (source.height - y - 1)) + (source.width - x - 1);
            image->data[dst_index] = source.data[src_index];
        }
    }
    image->width = source.width;
    image->height = source.height;
}

void rotate_image_90(struct image *image, struct image source)
{
    for (uint64_t y = 0; y < source.height; y++)
    {
        for (uint64_t x = 0; x < source.width; x++)
        {
            uint64_t src_index = (y * source.width) + x;
            uint64_t dst_index = ((source.width - x - 1) * source.height) + y;
            image->data[dst_index] = source.data[src_index];
        }
    }
    image->width = source.height;
    image->height = source.width;
}

void rotate_image_360(struct image *image, struct image source)
{
    for (uint64_t y = 0; y < source.height; y++)
    {
        for (uint64_t x = 0; x < source.width; x++)
        {
            uint64_t src_index = (y * source.width) + x;
            image->data[src_index] = source.data[src_index];
        }
    }
    image->width = source.width;
    image->height = source.height;
}

struct image_optional rotate(struct image const source, int32_t angle)
{

    struct image_optional image_optional;

    (void)angle;
    struct image new_image;
    uint64_t size = source.height * sizeof(struct pixel) * source.width;

    new_image.data = malloc(size);
    
    if (new_image.data == NULL){
        image_optional.status = MEMORY_ERROR;
        return image_optional;
    }

    new_image.height = source.height;
    new_image.width = source.width;


    if (angle < 0){
        angle = 360 - abs(angle) % 360;
    }

    switch (angle % 360)
    {
    case 90:
        rotate_image_90(&new_image, source);
        break;
    case 180:
        rotate_image_180(&new_image, source);
        break;
    case 270:
        rotate_image_270(&new_image, source);
        break;
    default:
        rotate_image_360(&new_image, source);
        break;
    }
    image_optional.status = OK;
    image_optional.img = new_image;
    return image_optional;
}
