#include "image_processing/bmp/serializing/write_bmp.h"

void fill_bmp_header(struct bmp_header *header, uint32_t width, uint32_t height)
{
    header->bfType = 0x4D42;
    header->bfileSize = sizeof(struct bmp_header) + width * height * 3;
    header->bfReserved = 0;
    header->bOffBits = sizeof(struct bmp_header);
    header->biSize = 40;
    header->biWidth = width;
    header->biHeight = height;
    header->biPlanes = 1;
    header->biBitCount = 24;
    header->biCompression = 0;
    header->biSizeImage = width * height * 3;
    header->biXPelsPerMeter = 0;
    header->biYPelsPerMeter = 0;
    header->biClrUsed = 0;
    header->biClrImportant = 0;
}

static uint32_t get_padding(uint32_t width)
{
    return (3 * width + 3) & (-4);
}

uint8_t to_bmp(FILE *out, struct image img)
{
    struct bmp_header bmp_header;
    fill_bmp_header(&bmp_header, img.width, img.height);

    if (out == NULL)
    {
        return FILE_DOES_NOT_EXIST;
    }

    if (!fwrite(&bmp_header, 1, sizeof(bmp_header), out))
    {
        return WRITE_ERROR;
    };
    uint64_t x = img.width;
    uint64_t y = img.height;

    uint32_t trash_size = get_padding(x) - x * 3;
    for (uint64_t i = 0; i < y; i++)
    {
        if (!fwrite(&img.data[i * x], 1, x * sizeof(struct pixel), out) ||
            !fwrite(&img.data[i * x], 1, trash_size, out))
        {
            return WRITE_ERROR;
        }
    }

    return WRITE_OK;
}
