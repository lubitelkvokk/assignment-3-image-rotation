#include "converter/bmp_to_image/bmp_to_image.h"
#include "converter/bmp_to_image/read_bmp.h"
#include "file_processing/open/bmp_file_open.h"
#include "file_processing/open/open_file_optional.h"
#include <stdio.h>
#include <stdlib.h>

// получаем файл и смещение, чтобы знать откуда считывать данные об изображении
int imageFilling(FILE *file, struct bmp_header *bmp_header, struct image *img)
{
    uint32_t x = bmp_header->biWidth;
    uint32_t y = bmp_header->biHeight;

    img->width = x;
    img->height = y;

    uint64_t image_row_capacity = x * sizeof(struct pixel);

    img->data = malloc(image_row_capacity * y);
    if (img->data == NULL)
    {
        return -1;
    }

    uint64_t trash_size = (-1 * (x * sizeof(struct pixel))) % 4;

    struct pixel *trash = malloc(sizeof(struct pixel) * trash_size);
    if (trash == NULL)
    {
        free(img->data);
        return -1;
    }
    for (uint64_t i = 0; i < y; ++i)
    {
        if (fread(&img->data[i * x], image_row_capacity, 1, file) != 1 ||
            fread(trash, trash_size, 1, file) != 1)
        {
            free(trash);
            free(img->data);
            return -1;
        };
    }

    // Освобождаем память, выделенную под мусор
    free(trash);

    return 0;
}

// С рассчётом того, что переданный файл точно не пустой делаем обработку остальных вариантов
struct bmp_optional from_bmp(FILE *file)
{
    struct bmp_optional bmp_optional;

    // Read the file header
    struct bmp_header file_header;

    // читаем данные
    if (fread(&file_header, sizeof(file_header), 1, file) != 1){
        bmp_optional.status = READ_EXCEPTION;
        return bmp_optional;
    }
    
    // Check the bitmap header

    if (file_header.biSize != 40)
    {
        bmp_optional.status = READ_INVALID_HEADER;
        return bmp_optional;
    }
    if (file_header.biBitCount != 24)
    {
        bmp_optional.status = READ_INVALID_BITS;
        return bmp_optional;
    }

    bmp_optional.status = READ_OK;
    int code = imageFilling(file, &file_header, &bmp_optional.img);
    if (code != 0)
    {
        bmp_optional.status = READ_MEMORY_ERROR;
    }
    return bmp_optional;
}
