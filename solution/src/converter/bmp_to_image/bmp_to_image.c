#include "bmp/deserializing/bmp_optional.h"
#include "converter/bmp_to_image/bmp_to_image.h"
#include "converter/bmp_to_image/read_bmp.h"
#include "file_processing/close/bmp_file_close.h"
#include "file_processing/open/bmp_file_open.h"
#include <stdlib.h>

void get_image_print_error(char *message, FILE *file)
{
    fprintf(stderr, "ERROR: %s", message);
    bmp_file_close(file);
}

struct bmp_optional get_image(const char *path)
{
    struct open_file_optional open_file_optional = bmp_file_open(path);
    switch (open_file_optional.status)
    {
    case FILE_IS_EMPTY:
        get_image_print_error("FILE IS EMPTY", open_file_optional.file);
        
    case NONEXISTING_FILE:
        get_image_print_error("FILE ISN'T EXIST", open_file_optional.file);
    default:
        if (open_file_optional.status != OPEN_OK)
        {
            get_image_print_error("UNDEFINED EXCEPTION WITH FILE OPENNING", open_file_optional.file);
        }
    }

    // получаем изображение
    struct bmp_optional bmp_optional = from_bmp(open_file_optional.file);
    // switch (bmp_optional.status)
    // {
    // case READ_INVALID_SIGNATURE:
    //     get_image_print_error("READ_INVALID_SIGNATURE", open_file_optional.file);
    // case READ_INVALID_BITS:
    //     get_image_print_error("READ_INVALID_BITS", open_file_optional.file);
    // case READ_INVALID_HEADER:
    //     get_image_print_error("READ_INVALID_HEADER", open_file_optional.file);
    // default:
    //     if (open_file_optional.status != OPEN_OK)
    //     {
    //         get_image_print_error("UNDEFINED EXCEPTION WITH IMAGE PROCESSING", open_file_optional.file);
    //     }
    // }
    
    // Оставляем обработку пользователю
    return bmp_optional;
}
