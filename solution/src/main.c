#include "converter/bmp_to_image/bmp_to_image.h"
#include "file_processing/close/bmp_file_close.h"
#include "file_processing/open/bmp_file_open.h"
#include "image_processing/bmp/serializing/write_bmp.h"
#include "transformation/rotate_image.h"
#include "util/util.h"
#include <stdio.h>
#include <stdlib.h>

#define INCORRECT_PARAMS 5
#define BMP_READ_ERROR 4
#define FILE_OPEN_ERROR 3
#define WRITE_ERROR 1
#define MEMORY_ERROR 2

void print_error(char *message)
{
    fprintf(stderr, "ERROR: %s", message);
}

int main(int argc, char **argv)
{

    if (argc != 4)
    {
        print_error("INCORRECT_PARAMS");
        return INCORRECT_PARAMS;
    }

    struct bmp_optional bmp_optional = get_image(argv[1]);

    if (bmp_optional.status != READ_OK)
    {
        print_error("BMP_READ_ERROR");
        return BMP_READ_ERROR;
    }

    struct image_optional rotated_image_optional = rotate(bmp_optional.img, atoi(argv[3]));
    if (rotated_image_optional.status != OK)
    {
        print_error("MEMORY_ERROR");
        exit(MEMORY_ERROR);
    }

    FILE *out = fopen(argv[2], "wb");
    if (!out)
    {
        print_error("FILE_OPEN_ERROR");
        return FILE_OPEN_ERROR;
    }
    uint8_t bmp_write_code = to_bmp(out, rotated_image_optional.img);
    if (bmp_write_code == WRITE_ERROR)
    {
        print_error("WRITE_ERROR");
        exit(WRITE_ERROR);
    }

    free_image(&bmp_optional.img);
    free_image(&rotated_image_optional.img);
    uint8_t bmp_close_code = bmp_file_close(out);
    if (bmp_close_code != CLOSE_OK)
    {
        print_error("BMP_FILE_CLOSE_ERROR");
    }
    return 0;
}
