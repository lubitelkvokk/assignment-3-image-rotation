#ifndef file_close_h
#define file_close_h

#include "file_processing/close/close_status.h"
#include "image_structs/bmp_header.h"
#include <stdio.h>

// Подгатавливает структуру close_file_optional.
// Он не делает обработку ошибок, чтобы отдать эту возможность
// вызывающей функции, позволяя самостоятельно выбрать способ обработки
uint8_t bmp_file_close(FILE *file);

#endif
