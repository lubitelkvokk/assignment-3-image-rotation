#ifndef open_status_h
#define open_status_h

#include <stdio.h>

enum open_status  {
  OPEN_OK,
  FILE_IS_EMPTY,
  NONEXISTING_FILE,
  OPEN_ERROR
  /* коды других ошибок  */
  };

#endif
