#ifndef file_open_h
#define file_open_h


#include "image_structs/bmp_header.h"
#include "image_structs/image.h"
#include "open_file_optional.h"
#include <stdio.h>

// Подгатавливает структуру open_file_optional.
// Он не делает обработку ошибок, чтобы отдать эту возможность 
// вызывающей функции, позволяя самостоятельно выбрать способ обработки
struct open_file_optional bmp_file_open(const char* filename);

#endif
