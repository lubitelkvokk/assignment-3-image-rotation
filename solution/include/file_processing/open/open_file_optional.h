#ifndef open_file_optional_h
#define open_file_optional_h


#include "open_status.h"
#include <stdio.h>

// Содержит информацию о статусе чтения файла
// Содержит указатель на файл
struct open_file_optional{
    enum open_status status;
    FILE* file;
};

#endif
