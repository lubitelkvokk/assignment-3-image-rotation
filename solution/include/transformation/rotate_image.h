#ifndef rotate_image_h
#define rotate_image_h

#include "image_processing/bmp/deserializing/image_optional.h"
#include "image_structs/image.h"


struct image_optional rotate( struct image const source, int32_t angle );

#endif
