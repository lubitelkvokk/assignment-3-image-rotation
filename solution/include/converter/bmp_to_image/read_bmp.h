#ifndef read_bmp_h
#define read_bmp_h


#include "bmp/deserializing/bmp_optional.h"
#include "image_structs/image.h"
#include <stdio.h>

struct bmp_optional from_bmp( FILE* file );

#endif
