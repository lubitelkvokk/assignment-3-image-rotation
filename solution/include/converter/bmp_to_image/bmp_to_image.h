#ifndef bmp_to_image_h
#define bmp_to_image_h

#include "bmp/deserializing/bmp_optional.h"
#include "image_structs/image.h"



struct bmp_optional get_image(const char* path);

#endif
