#ifndef write_status_h
#define write_status_h

enum write_status{
    WRITE_OK,
    FILE_DOES_NOT_EXIST,
    WRITE_ERROR
};

#endif