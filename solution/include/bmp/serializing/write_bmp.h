#ifndef reab_bmp_h
#define read_bmp_h

#include <stdio.h>
#include "../../../image_structs/image.h"
#include "write_status.h"
#include "../../../image_structs/bmp_header.h"

enum write_status to_bmp( FILE* in, struct image img );
    
#endif