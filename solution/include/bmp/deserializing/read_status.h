#ifndef read_status_h
#define read_status_h


// Содержит статусы прочитанного файла
enum read_status  {
  READ_OK,
  READ_INVALID_SIGNATURE,
  READ_INVALID_BITS,
  READ_INVALID_HEADER,
  READ_EXCEPTION,
  READ_MEMORY_ERROR
  };


#endif
