#ifndef bmp_optional_h
#define bmp_optional_h

#include "image_structs/image.h"
#include "read_status.h"

// Содержит статус преобразования содержимого bmp файла в struct image и саму struct image
struct bmp_optional{
    enum read_status status;
    struct image img;
};

#endif
