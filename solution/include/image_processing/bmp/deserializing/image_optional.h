#ifndef image_optional_h
#define image_optional_h

#include "image_processing/bmp/deserializing/image_processing_status.h"
#include "image_structs/image.h"



// Содержит статус преобразования содержимого bmp файла в struct image и саму struct image
struct image_optional{
    enum image_processing_status status;
    struct image img;
};


#endif
