#ifndef image_processing_status_h
#define image_processing_status_h


// Содержит статусы прочитанного файла
enum image_processing_status  {
  OK,
  MEMORY_ERROR
  /* коды других ошибок  */
  };


#endif
