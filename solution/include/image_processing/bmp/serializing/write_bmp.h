#ifndef read_bmp_h
#define read_bmp_h

#include "image_processing/bmp/serializing/write_status.h"
#include "image_structs/bmp_header.h"
#include "image_structs/image.h"
#include <stdio.h>

uint8_t to_bmp( FILE* out, struct image img );
    
#endif
