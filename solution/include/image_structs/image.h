#ifndef image_h
#define image_h

#include <stdint.h>

struct image
{
  uint64_t width, height;
  struct pixel *data;
  // struct pixel **dataRow;
};

struct pixel
{
  uint8_t b, g, r;
};

#endif
